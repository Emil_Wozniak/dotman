# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi


# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -f `which powerline-daemon` ]; then
  powerline-daemon -q
  POWERLINE_BASH_CONTINUATION=1
  POWERLINE_BASH_SELECT=1
  . /usr/share/powerline/bash/powerline.sh
fi

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/emil/.sdkman"
[[ -s "/home/emil/.sdkman/bin/sdkman-init.sh" ]] && source "/home/emil/.sdkman/bin/sdkman-init.sh"

source ~/.zshrc 
export PATH=/bin/lscript:/bin/lscript:/home/emil/.anaconda/bin:/home/emil/.anaconda/condabin:/home/emil/.sdkman/candidates/maven/current/bin:/home/emil/.sdkman/candidates/kscript/current/bin:/home/emil/.sdkman/candidates/kotlin/current/bin:/home/emil/.sdkman/candidates/java/current/bin:/home/emil/.sdkman/candidates/gradle/current/bin:/usr/share/Modules/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/home/emil/bin:/var/lib/snapd/snap/bin:/home/emil/bin
export PATH=/home/emil/dev/bin/:/bin/lscript:/bin/lscript:/home/emil/.anaconda/bin:/home/emil/.anaconda/condabin:/home/emil/.sdkman/candidates/maven/current/bin:/home/emil/.sdkman/candidates/kscript/current/bin:/home/emil/.sdkman/candidates/kotlin/current/bin:/home/emil/.sdkman/candidates/java/current/bin:/home/emil/.sdkman/candidates/gradle/current/bin:/usr/share/Modules/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/home/emil/bin:/var/lib/snapd/snap/bin:/home/emil/bin
